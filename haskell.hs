fac 0 = 1
fac n = n * fac (n - 1)

n = 1000 * 100
a = [fac i | i <- [1..n]]
main = putStrLn $ "Hello world! " ++ show (a !! 0)
