#include <stdio.h>
long fac(int n);

long fac(int n) {
	if (n == 0) return 1;

	long res = n;
	while(n > 0) {
		res = res * --n;
	}
	return res;
}

int main(void) {
	int n = 1000 * 100;
	long a[n];
	int i;
	for(i = 0; i < n; i++) {
		a[i] = fac(42 * i);
	}
	printf("Hello world! %l", a[0]);
	return 0;
}
