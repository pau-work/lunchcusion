C=c.c
CO=c.o 
CEXE=c.out 

HS=haskell.hs
HSO=haskell.o
HSEXE=haskell.out

TIME=timeit.sh

$(TIME) : $(HSEXE) $(CEXE)
	bash timeit.sh

$(HSEXE) : $(HS)
	ghc -o $(HSEXE) $(HS)

$(CEXE) : $(CO)
	gcc -o $(CEXE) $(CO)

main.o : $(C)
	gcc -c $(C)

.PHONY : clean
clean:
	-rm $(CO) $(CEXE) $(HSO) $(HEXE)
